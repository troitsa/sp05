package ru.vlasova.taskmanager.model.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Getter
@Setter
@NoArgsConstructor
public final class ProjectDTO extends ItemDTO {


}
