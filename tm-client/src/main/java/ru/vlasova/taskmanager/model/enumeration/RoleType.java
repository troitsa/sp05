package ru.vlasova.taskmanager.model.enumeration;

public enum RoleType {
    USER,
    ADMIN
}
