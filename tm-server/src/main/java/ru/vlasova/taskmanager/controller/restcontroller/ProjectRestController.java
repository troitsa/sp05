package ru.vlasova.taskmanager.controller.restcontroller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import ru.vlasova.taskmanager.api.service.IProjectService;
import ru.vlasova.taskmanager.api.service.IUserService;
import ru.vlasova.taskmanager.model.dto.ProjectDTO;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ProjectRestController {

    private final IProjectService projectService;

    private final IUserService userService;

    @Autowired
    public ProjectRestController(IProjectService projectService, IUserService userService) {
        this.projectService = projectService;
        this.userService = userService;
    }

    @GetMapping("/rproject/{id}")
    public ResponseEntity<ProjectDTO> getProject(@NotNull final Authentication authentication,
                                                 @Nullable @PathVariable("id") final String id) {
        @Nullable final ProjectDTO projectDTO = projectService
                .toProjectDTO(projectService.findOne(id, getCurrentUserId(authentication)));
        return projectDTO != null
                ? new ResponseEntity<>(projectDTO, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping(value = "/rproject")
    public ResponseEntity<?> createProject(@NotNull final Authentication authentication,
                                           @Nullable @RequestBody final ProjectDTO project) {
        projectService.merge(projectService.toProject(project));
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(value = "/rprojects")
    public ResponseEntity<List<ProjectDTO>> getProjects(@NotNull final Authentication authentication) {
        @NotNull final List<ProjectDTO> projects = projectService
                .findAll()
                .stream()
                .map(projectService::toProjectDTO)
                .collect(Collectors.toList());
        return !projects.isEmpty()
                ? new ResponseEntity<>(projects, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PutMapping(value = "/rproject/{id}")
    public ResponseEntity<?> updateProject(@NotNull final Authentication authentication,
                                           @NotNull @PathVariable(name = "id") final String id,
                                           @NotNull @RequestBody final ProjectDTO project) {
        if (projectService.findOne(id, getCurrentUserId(authentication)) != null) {
            projectService.merge(projectService.toProject(project));
            return new ResponseEntity<>(HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_MODIFIED);
    }

    @DeleteMapping(value = "/rproject/{id}")
    public ResponseEntity<?> deleteProject(@NotNull final Authentication authentication,
                                           @NotNull @PathVariable(name = "id") final String id) {
        projectService.remove(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(value = "/rprojects/search/{keyword}")
    public ResponseEntity<List<ProjectDTO>> searchProjects(@NotNull Authentication authentication,
                                                           @NotNull @PathVariable(name = "keyword") final String keyword) {
        @Nullable final List<ProjectDTO> projects = projectService
                .search(keyword, getCurrentUserId(authentication))
                .stream()
                .map(projectService::toProjectDTO)
                .collect(Collectors.toList());
        return !projects.isEmpty()
                ? new ResponseEntity<>(projects, HttpStatus.OK)
                : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    private String getCurrentUserId(@NotNull final Authentication auth) {
        return userService.findByUsername(auth.getName()).getId();
    }

}
